package com.wisercare.expectedvalue;


import lombok.Data;

@Data
//an outcome is a possible result of treatment, it could be a negative side effect or a positive benefit
//outcomes are related to treatments that cause them through probabilities
public class Outcome {
    private String name;
    private String description;

    public Outcome(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (!(o instanceof Outcome)) {
            return false;
        }

        Outcome other = (Outcome)o;
        return this.name.equals(other.name);
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }
}
