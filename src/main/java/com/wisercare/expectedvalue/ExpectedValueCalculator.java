package com.wisercare.expectedvalue;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class ExpectedValueCalculator
{

    public List<TreatmentExpectedValue> processValuesForTreatments(
            List<Treatment> treatments,
            List<PatientPreference> patientPreferences,
            ProbabilitySet probabilitySet)
    {
        if (treatments == null || patientPreferences == null || probabilitySet == null) {
            throw new IllegalArgumentException();
        }
        /*
        with the information passed in to this method, it's possible to calculate an "Expected Value"

        which is the sum of all of the individual results of
          multiplying each outcome preference value (how much the patient cares about a thing)
          with that outcome's probability in relation to a treatment (how likely the thing is to happen)

        //Example: if you have 3 possible outcomes (A,B,C) for treatment, the expected value would be:
           (chance of outcome A * preference of outcome A)
         + (chance of outcome B * preference of outcome B)
         + (chance of outcome C * preference of outcome C)

        These treatment expected values should be sorted in descending order, with the treatment that will provide the
        greatest relative value displayed first, and the treatment that will provide the least expected value last.
        */

        List<TreatmentExpectedValue> results = new ArrayList<>();
        for (Treatment treatment: treatments) {
            double expectedValue = 0;
            for (PatientPreference patientPreference: patientPreferences) {
                Outcome outcome = patientPreference.getOutcome();
                Double preferenceValue = patientPreference.getPreferenceValue();
                Double percentChance = probabilitySet.getPercentChance(treatment, outcome);
                expectedValue += preferenceValue * percentChance;
            }

            TreatmentExpectedValue treatmentExpectedValue = new TreatmentExpectedValue(treatment, expectedValue);
            results.add(treatmentExpectedValue);
        }
        
        Collections.sort(results, (p, q) ->
            {
                if (q.getExpectedValue().doubleValue() > p.getExpectedValue()) {
                    return 1;
                }
                else if (q.getExpectedValue().doubleValue() < p.getExpectedValue()) {
                    return -1;
                }
                else {
                    return 0;
                }
            });
        return results;
    }
}
