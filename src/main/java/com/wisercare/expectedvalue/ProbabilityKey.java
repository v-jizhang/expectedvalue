package com.wisercare.expectedvalue;


import lombok.Data;

@Data
public class ProbabilityKey {
    private Treatment treatment;
    private Outcome outcome;

    public ProbabilityKey(Treatment treatment, Outcome outcome) {
        this.treatment = treatment;
        this.outcome = outcome;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if( o == null) {
            return false;
        }
        if (!(o instanceof ProbabilityKey)) {
            return false;
        }

        ProbabilityKey other = (ProbabilityKey) o;
        return this.treatment.equals(other.treatment) && this.outcome.equals(other.outcome);
    }

    @Override
    public int hashCode() {
        return treatment.hashCode() * outcome.hashCode();
    }
}
