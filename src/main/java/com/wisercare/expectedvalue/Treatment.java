package com.wisercare.expectedvalue;

import lombok.Data;

@Data
//a treatment is some sort of medical intervention (or non-intervention) for a particular condition
//treatments are related to potential outcomes through their probabilities
public class Treatment {
    private String name;
    private String description;

    public Treatment(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (!(o instanceof Treatment)) {
            return false;
        }

        Treatment other = (Treatment)o;
        return this.name.equals(other.name);
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }
}
